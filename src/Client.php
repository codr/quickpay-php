<?php

namespace Codr\Quickpay;

use Codr\Curl\Request;

class Client
{

    protected $key;
    protected $headers;

    private $url = "https://api.quickpay.net";

    private $request;
    private $response;

    public function __construct($key)
    {
        $this->key = $key;
        $headers = [
            "Accept-Version" => "v10"
        ];

        $this->request = new Request();
        $this->request->setHeaders($headers);
        $this->request->setBasicAuth(null, $key);

    }

    public function get()
    {

        return $this->exec('GET');
    }

    public function post()
    {

        return $this->exec('POST');
    }

    public function put()
    {

        return $this->exec('PUT');
    }

    public function patch()
    {

        return $this->exec('PATCH');
    }

    public function delete()
    {

        return $this->exec('DELETE');
    }

    protected function exec($method)
    {

        $this->request->request($method, $this->url);

        return null;
    }
}