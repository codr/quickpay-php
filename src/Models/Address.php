<?php

/**
 * Created by PhpStorm.
 * User: jc
 * Date: 27/10/15
 * Time: 21:39
 */

namespace Codr\Quickpay\Models;


class Address
{
    protected $name;
    protected $att;
    protected $street;
    protected $city;
    protected $zip_code;
    protected $region;
    protected $country_code;
    protected $vat_no;

    public function __construct()
    {

    }
}