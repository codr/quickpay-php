<?php

/**
 * Created by PhpStorm.
 * User: jc
 * Date: 27/10/15
 * Time: 21:30
 */

namespace Codr\Quickpay\Models;

class Properties
{

    protected $properties;
    protected $changed;

    protected $exists;

    public function __construct(array $properties)
    {
        $this->fill($properties);
    }


    public function setExists($bool)
    {
        $this->exists = $bool;
    }

    public function fill($properties)
    {
        foreach ($properties as $key => $value)
            $this->properties[$key] = $value;
    }

    protected function setProperty($key, $value)
    {
        if ($this->exists)
            $this->changed[$key] = $value;

        $this->properties[$key] = $value;
    }

    protected function getProperty($key)
    {
        if (array_key_exists($key, $this->properties))
            return $this->properties[$key];

        return false;
    }

    public function __set($key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->properties))
            return $this->properties[$key];

        return false;
    }
}