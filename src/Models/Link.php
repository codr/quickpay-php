<?php

namespace Codr\Quickpay\Models;

class Link
{

    /**
     * Payment url
     * @var string
     */
    protected $url;

    protected $properties = [];
    protected $changed = [];

    private $exists;

    public function __construct($properties = [])
    {
        $this->setExists(false);
        $this->fill($properties);
    }

    /**
     * Get QuickPay payment link
     * @return string 
     */
    public function get()
    {
        if (!isset($this->url)) {

        }

        return $this->url;
    }

    public function getChanged()
    {
        return array_diff_key($this->changed, $this->properties);
    }

    public function setExists($bool)
    {
        $this->exists = $bool;
    }

    public function fill($properties)
    {
        foreach ($properties as $key => $value)
            $this->properties[$key] = $value;
    }

    protected function setProperty($key, $value)
    {
        if ($this->exists)
            $this->changed[$key] = $value;

        $this->properties[$key] = $value;
    }

    protected function getProperty($key)
    {
        if (array_key_exists($key, $this->properties))
            return $this->properties[$key];

        return false;
    }

    public function __set($key, $value)
    {
        $this->setProperty($key, $value);
    }

    public function __get($key)
    {
        return $this->getProperty($key);
    }
}