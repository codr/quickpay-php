<?php

/**
 * Created by PhpStorm.
 * User: jc
 * Date: 27/10/15
 * Time: 21:56
 */

namespace Codr\Quickpay\Models;


class Operation
{
    const APPROVED = "20000"; // Approved
    const REJECTED = "40000"; // Rejected by acquirer
    const REQUEST_ERROR = "40001"; // Request data error
    const GATEWAY_ERROR = "50000"; // Gateway error
    const COMMUNICATION_ERROR = "50300"; // Communication error with acquirer

    protected $properties = [];

    public function __construct(array $data)
    {
        $this->fill($data);
    }

    public function isApproved()
    {
        return $this->qp_status_code == self::APPROVED;
    }

    public function fill($properties)
    {
        foreach ($properties as $key => $value) {
            $this->properties[$key] = $value;
        }
    }

    protected function getProperty($key)
    {
        if (array_key_exists($key, $this->properties))
            return $this->properties[$key];

        return false;
    }

    public function __get($key)
    {
        return $this->getProperty($key);
    }
}