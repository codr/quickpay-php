<?php
namespace Codr\Quickpay;

use Codr\Curl\Request;
use Codr\Quickpay\Models\Link;

class Subscription
{

    /**
     * QuickPay API key
     * @var string
     */
    public static $apikey;

    /**
     * Quickpay API url
     * @var string
     */
    public static $apiurl;

    /**
     * Merchant private key;
     * @var string
     */
    public static $private_key;

    /**
     * @var array
     */
    private $metadata;

    public $link;
    /**
     * Exists in quickpay records
     * @var bool
     */
    private $exists;

    private $params = [];

    private $properties = [];
    private $changed = [];

    public function __construct($params = [])
    {
        $this->setExists(false);

        $this->fill($params);
    }

    protected function fill(array $params)
    {
        foreach ($params as $key => $value)
            $this->setParam($key, $value);

        return $this;
    }

    protected function setExists($bool)
    {
        $this->exists = $bool;

        return $this;
    }

    protected function setParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    protected function getParam($key)
    {
        if (array_key_exists($key, $this->params))
            return $this->params[$key];

        return false;
    }

    protected function unsetParam($key)
    {
        if (array_key_exists($key, $this->params))
            unset($this->params[$key]);
    }

    private function execute($method, $endpoint, $data)
    {
        if (!isset(self::$apikey) && !isset(self::$apiurl)) {
            throw new \Exception("Missing apikey or url");
        }

        $req = new Request();
        $req->setBasicAuth(null, self::$apikey);
        $req->setHeaders("Accept-Version", "v10");

        $res = $req->request($method, self::$apiurl . $endpoint, array_merge($data, ["id" => $this->id]));
        $body = json_decode($res->getBody(), true);

        if (preg_match("/20[0-4]/", $res->getCode())) {
            return $body;
        } else {
            throw new \Exception($body["message"]);
        }
    }

    public static function isValid($data, $checksum)
    {
        $hash = hash_hmac("sha256", $data, self::$private_key);

        return $hash == $checksum;
    }

    public function __get($key)
    {
        return $this->getParam($key);
    }
}