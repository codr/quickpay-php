<?php

/**
 * Created by PhpStorm.
 * User: jc
 * Date: 26/10/15
 * Time: 15:07
 */

namespace Codr\Quickpay\Structs;

class StructOverload
{
    protected $container = [];

    public function __call($method, $value)
    {
        if (!$this->isSetterMethod($method))
            return;

        $prop = strtolower(substr($method, 3));

        $this->container[$prop] = $value[0];
    }

    public function __get($prop)
    {
        return array_key_exists($prop, $this->container) ? $this->container[$prop] : null;
    }

    protected function setOverloadProperties(array $props)
    {
        foreach ($props as $k => $v) {
            $this->container[strtolower($v)] = null;
        }
    }

    private function isSetterMethod($method)
    {
        if (strcmp("set", substr($method, 0, 3)) === 0) {
            return true;
        }
        return false;
    }
}