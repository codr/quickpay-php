<?php

/**
 * Created by PhpStorm.
 * User: jc
 * Date: 05/10/15
 * Time: 15:08
 */

namespace Codr\Quickpay\Structs;

class Address extends StructOverload implements \JsonSerializable
{
    function __construct(array $address = [])
    {
        $this->setOverloadProperties(
            ["name", "att", "street", "city", "zip_code", "region", "country_code", "vat_no"]
        );

        foreach ($address as $key => $val) {
            call_user_func_array([$this, "set" . $key], [$val]);
        }
    }

    public function jsonSerialize()
    {
        return json_encode($this->container);
    }
}