<?php

/**
 * Created by PhpStorm.
 * User: jc
 * Date: 22/10/15
 * Time: 15:59
 */

namespace Codr\Quickpay\Collections;


class Collection implements \Countable, \ArrayAccess
{
    protected $container = array();

    public function offsetGet($key)
    {
        return isset($this->container[$key]);
    }

    public function offsetSet($key, $value)
    {
        if (is_null($key)) {
            array_push($this->container, $value);
        } else {
            $this->container[$key] = $value;
        }
    }

    public function offsetUnset($key)
    {
        unset($this->container[$key]);
    }
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->container);
    }

    public function count()
    {
        return count($this->container);
    }
}